<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Costumer;

class CostumerController extends Controller
{
    public function data(Request $request)
    {
            $costumer= Costumer::where('name', 'like', '%'. $request->search. '%');
            return $costumer->paginate(100,['*'], 'page', $request->page);
    }
    public function index(){
        return view('welcome');
    }
}

