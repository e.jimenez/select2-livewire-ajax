<div>
    <form wire:submit.prevent="enviar">
        <div wire:ignore>
            <select class="form-control" id="costumer"></select>
            <br> <br>
            <button class="btn btn-primary">enviar</button>
        </div>

    </form>
    @push('scripts')
        <script>
            $(document).ready(function() {
                $('#costumer').select2({
                    ajax: {
                        url: '{{ URL('costumer') }}',
                        type: 'get',
                        dataType: 'json',
                        delay: 250,
                        data: function(params) {
                            return {
                                search: params.term,
                                page: params.page
                            }
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.data,
                                pagination: {
                                    more: data.last_page != params.page
                                }
                            }
                        },
                        cache: true,
                    },
                    placeholder: 'Clientes',
                    templateResult: templateResult,
                    templateSelection: templateSelection,
                });
                $('#costumer').on('change', function(e) {
                    var data = $('#costumer').select2("val");
                    @this.set('name', data);

                })
            });


            function templateResult(data) {
                if (data.loading) {
                    return data.text
                }
                return data.name
            }

            function templateSelection(data) {
                return data.name;
            }
        </script>
    @endpush
</div>
